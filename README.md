Some of my configs that I need to share across machines 

# Use `stow` to manage the packages 

```
stow -t <path to link to> <package name >
```

For example 

```
stow -t /home/therapon/ Vim
```
will setup all the vim related confs found in `Vim` folder