;;(package-initialize)
;;(require 'package)

(setq gc-cons-threshold 100000000)

;; Add package archives melpa and gnu
;; (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
;; (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))

;; refresh package list
;;(when (not package-archive-contents)
;; (package-refresh-contents))

;; ensure we have `use-package' installed
;;(unless (package-installed-p 'use-package)
;;  (package-install 'use-package))

;; enable :ensure by default for use-package
;;(require 'use-package-ensure)
;;(setq use-package-always-ensure t)

;; using straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-use-package-by-default t)

(defconst my-elisp-path (concat user-emacs-directory "elisp"))
(add-to-list 'load-path my-elisp-path) ; add my elisp dir



;; Restore after startup
(add-hook 'after-init-hook
      (lambda ()
        (setq gc-cons-threshold 1000000)
        (message "gc-cons-threshold restored to %S"
             gc-cons-threshold)))

(straight-use-package 'org)
;; Load theme first to allow my overrides to occur later.
(use-package zenburn-theme
  :config
  (load-theme 'zenburn t))

(use-package tsk-emacs-base
  :straight nil
  :load-path "~/.emacs.d/elisp/tsk-emacs-base.el")

(use-package tsk-emacs-common-extensions
  :straight nil
  :load-path "~/.emacs.d/elisp/tsk-emacs-common-extensions.el")

(use-package tsk-org
  :straight nil
  :load-path "~/.emacs.d/elisp/tsk-org.el")

(use-package tsk-latex
  :straight nil
  :load-path "~/.emacs.d/elisp/tsk-latex.el")

(use-package tsk-evil
  :straight nil
  :load-path "~/.emacs.d/elisp/tsk-evil.el")

(use-package rustic
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  ;; uncomment for less flashiness
  ;; (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  (setq rustic-format-on-save t)
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm, but don't try to
  ;; save rust buffers that are not file visiting. Once
  ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
  ;; no longer be necessary.
  (when buffer-file-name
    (setq-local buffer-save-without-query t)))

(use-package lsp-mode
  :commands lsp
  :custom
  ;; what to use when checking on-save. "check" is default, I prefer clippy
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  ;; enable / disable the hints as you prefer:
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints nil)
  (lsp-rust-analyzer-display-reborrow-hints nil)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))

(use-package lsp-ui
  :commands lsp-ui-mode
  :bind
  (("C-c C-l d" . lsp-ui-doc-show)
   ("C-c C-l q" . lsp-ui-doc-hide))
  :custom
  (lsp-eldoc-render-all nil)
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-doc-enable nil)
  (lsp-ui-doc-position 'at-point ))

;; (use-package company-lsp)

(use-package adoc-mode
  :mode ("\\.adoc\\|\\.asciidoc\\'" . adoc-mode)
  :hook ((adoc-mode . turn-on-flyspell)
	 (adoc-mode . turn-on-auto-fill))
)

;; java
;;(use-package lsp-java
;;  :after lsp
;;  :bind (("\C-\M-b" . lsp-find-implementation)
;;         ("M-RET" . lsp-execute-code-action))
;;  :config
;;  (add-hook 'java-mode-hook 'lsp)
;;  (setq lsp-java-vmargs
;;        (list
;;         "-noverify"
;;         "-Xmx1G"
;;
;;         "-XX:+UseG1GC"
;;         "-XX:+UseStringDeduplication"
;;         "-javaagent:/home/torstein/.m2/repository/org/projectlombok/lombok/1.18.6/lombok-1.18.6.jar"
;;         )
;;        ;; Don't organise imports on save
;;        lsp-java-save-action-organize-imports nil
;;        ;; Currently (2019-04-24), dap-mode works best with Oracle
;;        ;; JDK, see https://github.com/emacs-lsp/dap-mode/issues/31
;;        ;; lsp-java-java-path "/opt/oracle-jdk-12.0.1/bin/java"
;;        )
;;  )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(orderless marginalia rustic use-package paradox org-modern modus-vivendi-theme modus-operandi-theme hc-zenburn-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
