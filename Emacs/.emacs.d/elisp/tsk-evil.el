;;; tsk-evil.el -- evil confs

;;; Commentary:

;;; Code:

(setq evil-want-keybinding nil)

(use-package evil
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  :config
  (evil-mode 1)
  (setq evil-want-fine-undo t)
  (evil-set-undo-system 'undo-redo)
)


(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))


(use-package treemacs-evil
  :after (treemacs evil)
  :ensure t)



(provide 'tsk-evil)
;;; tsk-evil.el ends here
