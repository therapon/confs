;;; package -- tex setup


;;; Commentary:
;;; Code:

(use-package reftex
  :config
  (setq reftex-cite-prompt-optional-args t))  ; Prompt for empty optional arguments in cite

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package pdf-tools				    ;;
;;   :ensure t						    ;;
;;   :mode ("\\.pdf\\'" . pdf-view-mode)		    ;;
;;   ;; :bind ("C-c C-g" . pdf-sync-forward-search)	    ;;
;;   :config						    ;;
;;   (setq mouse-wheel-follow-mouse t			    ;;
;; 	pdf-annot-activate-created-annotations t)	    ;;
;;   (pdf-tools-install :no-query)			    ;;
;;   (setq pdf-view-resize-factor 1.10))		    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


 (use-package auctex
   :straight t
   :mode ("\\.tex\\'" . latex-mode)
   :config
   (setq TeX-auto-save t
	 TeX-parse-self t
	 TeX-source-correlate-mode t
	 ;; TeX-source-correlate-method 'synctex
	 reftex-plug-into-AUCTeX t
	 TeX-source-correlate-start-server t
	 font-latex-fontify-script nil
	 tex-fontify-script nil
	 font-latex-fontify-sectioning 'color
	 TeX-PDF-mode t)
   (setq-default TeX-master nil)
   (add-hook 'LaTeX-mode-hook
	     (lambda ()
	       (company-mode)
	       (smartparens-mode)
	       (turn-on-reftex)
	       (visual-line-mode)
	       (LaTeX-math-mode)
	       (flyspell-mode)
	       (reftex-isearch-minor-mode)))

   ;; Update PDF buffers after successful LaTeX runs
   (add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
 	    #'TeX-revert-document-buffer)

   ;; to use pdfview with auctex
   (add-hook 'LaTeX-mode-hook 'pdf-tools-install)

   ;; to use pdfview with auctex
   (setq TeX-view-program-selection '((output-pdf "pdf-tools"))
 	TeX-source-correlate-start-server t)
   (setq TeX-view-program-list '(("pdf-tools" TeX-pdf-tools-sync-view))))

;; set latex as alternate input
(setq default-input-method 'TeX)

(use-package org-noter
  :defer t)

(require 'auctex)      ;; force loading of config
(provide 'tsk-latex)
;;; tsk-latex.el ends here
