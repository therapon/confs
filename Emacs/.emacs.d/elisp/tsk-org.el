;;; package --- tsk-org
;;; Commentary:
;;;  org-mode related configs

;;; Code:


;; color packages first to allow customizations later.
(use-package org-modern
  :config
  (setq org-modern-todo-faces
	(quote (("TODO"      :height 1.3  :foreground "white"    :weight bold)
		("INPROG"    :height 1.3  :foreground "green"    :weight bold)
		("WAITING"   :height 1.3  :foreground "orange"   :weight bold)
		("CANCELLED" :height 1.3  :foreground "magenta"  :weight bold)
		("DONE"      :height 1.3  :foreground "black"    :weight bold)
		)))
  (global-org-modern-mode))


(setq tsk-org-dir "~/Repos/GitLab/Personal/tm/org"
      tsk-org-targets-dir (concat tsk-org-dir "/todos")
)

(defun tsk-org--inbox ()
  (concat tsk-org-targets-dir "/inbox.org"))


(defun tsk-org--refile-targets ()
  "Return the list of files to be used for refilling targets."
  (if (file-directory-p tsk-org-targets-dir)
      (directory-files tsk-org-targets-dir t "[^ \.?\.\.]")
    (lwarn :warning "Unable to find org-targets-dir: %s" tsk-org-targets-dir)))

(use-package org
  :mode ("\\.org\\'" . org-mode)
  :hook (org-mode . turn-on-flyspell)
  :bind (("C-c l" . org-store-link)
	 ("C-c c" . org-capture)
	 ("M-[" . org-mark-ring-goto)
	 ("C-c a" . org-agenda)
	 ("C-c b" . org-iswitchb)
	 ("C-c C-w" . org-refile)
         ("C-c j" . org-clock-goto)
         ("C-c C-x C-o" . org-clock-out))
  :config
  (add-hook 'org-mode-hook 'turn-on-auto-fill)
  (add-hook 'org-mode-hook #'visual-line-mode)
  (add-hook 'org-mode-hook #'org-indent-mode)
  ;; The GTD part of this config is heavily inspired by
  ;; https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html
  (setq  org-directory tsk-org-dir
	 org-default-notes-file (tsk-org--inbox)
	 org-agenda-files `(,tsk-org-targets-dir)
	 org-hide-leading-stars t
	org-log-done 'time
	org-src-fontify-natively t
	org-use-speed-commands t
	org-capture-templates
	'(("t" "Tasks [inbox]" entry
	   (file+headline "" "Inbox")
	   "* TODO %i%?
:PROPERTIES:
:CREATED: %U
:END:" :empty-lines 1
)
	  )
	 org-refile-targets
	 `((,(tsk-org--refile-targets) :maxlevel . 5))
	org-todo-keywords
	'((sequence "TODO(t)" "INPROG(p)" "WAITING(w@/!)" "|" "DONE(d)" "CANCELLED(c@/!)")
	  )
	 org-todo-keyword-faces
	 '(("TODO" :foreground "green" :weight normal)
	   ("INPROG" :foreground "green" :weight bold)
	   ("WAITING" :foreground "yellow")
	   ("DONE" :foreground "orange")
	   ("CANELLED" :foreground "magenta")
	   )
	;; ;; org-agenda-custom-commands
        ;; '(("@" "Contexts"
	;;    ((tags "@Amazon")
	;;     (tags-todo "other"))))
        org-agenda-restore-windows-after-quit t
	org-log-into-drawer t
	org-agenda-compact-blocks t
	org-use-fast-todo-selection t
	org-treat-S-cursor-todo-selection-as-state-change nil
	org-clock-persist t
	org-time-clocksum-format
	'(:hours "%d" :require-hours t
		 :minutes ":%02d" :require-minutes t)
	org-blank-before-new-entry '((heading . t) (plain-list-item . auto))
	)
  (org-clock-persistence-insinuate)
  (org-babel-do-load-languages
   'org-babel-load-languages '((emacs-lisp . t)
			       (java . t)
			       (python . t)))

  )


(setq calendar-latitude 47.68)
(setq calendar-longitude -122.34)
(setq calendar-location-name "Seattle, WA")

(use-package org-ref)
;; Trying org-roam

;; (setq org-roam-directory (concat (getenv "HOME") "/Org/Roam"))
;;
;; (use-package org-roam
;;   :after org
;;   :init (setq org-roam-v2-ack t)
;;   :custom
;;   (org-roam-directory (file-truename org-roam-directory))
;;   (org-roam-completion-everywhere t)
;;   :config
;;   (org-roam-setup)
;;   :bind (("C-c n f" . org-roam-node-find)
;; 	 ("C-c n r" . org-roam-node-random)
;; 	 (:map org-mode-map
;; 	       (("C-c n i" . org-roam-node-insert)
;; 		("C-c n o" . org-id-get-create)
;; 		("C-c n t" . org-roam-tag-add)
;; 		("C-c n a" . org-roam-alias-add)
;; 		("C-M-i" . completion-at-point)
;; 		("C-c n l" . org-roam-buffer-toggle)))))
;;

;; Consult for consult-org-roam and its search functionality.
;; (use-package consult-org-roam
;;    :ensure t
;;    :init
;;    (require 'consult-org-roam)
;;    ;; Activate the minor-mode
;;    (consult-org-roam-mode 1)
;;    :custom
;;    (consult-org-roam-grep-func #'consult-ripgrep)
;;    :config
;;    ;; Eventually suppress previewing for certain functions
;;    (consult-customize
;;     consult-org-roam-forward-links
;;     :preview-key (kbd "M-."))
;;    :bind
;;    ("C-c n e" . consult-org-roam-file-find)
;;    ("C-c n b" . consult-org-roam-backlinks)
;;    ("C-c n r" . consult-org-roam-search))




(provide 'tsk-org)
;;; tsk-org.el ends here
