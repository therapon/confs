" An example for a gvimrc file.
" The commands in this are executed when the GUI is started.
"
" $Author: skotthe $
" $Date: 2004/11/16 23:32:47 $
" $Log: .vimrc,v $
" Revision 1.2  2004/11/16 23:32:47  skotthe
" *** empty log message ***
"
" From https://w.amazon.com/?ApolloVim
" source /apollo/env/envImprovement/var/vimrc

" Make external commands work through a pipe instead of a pseudo-tty
"set noguipty

" set the X11 font to use
" set guifont=Courier\ 10\ Pitch\ 10
" set guifont=Monospace\ 12
set guifont=DejaVu\ Sans\ Mono\ 10

" Backspace is broken in Amazon
if &term == "xterm" || &term == "screen" 
    set t_kb=
    fixdel
endif

" antialias fonts with vim -- does not work on command line for Mac 
" set nomacatsui anti termencoding=macroman gfn=Anonymous:h12

" Make command line two lines high
set ch=2

" Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

" Only do this for Vim version 5.0 and later.
if version >= 500

  " I like highlighting strings inside C comments
  let c_comment_strings=1

  " Switch on syntax highlighting.
  syntax on
  colorscheme desert

  " Switch on search pattern highlighting.
  set incsearch 

  " For Win32 version, have "K" lookup the keyword in a help file
  "if has("win32")
  "  let winhelpfile='windows.hlp'
  "  map K :execute "!start winhlp32 -k <cword> " . winhelpfile <CR>
  "endif

  " Hide the mouse pointer while typing
  set mousehide

  " Set nice colors
  " background for normal text is light grey
  " Text below the last line is darker grey
  " Cursor is green
  " Constants are not underlined but have a slightly lighter background

  set guioptions-=T
  set ru
  set showmatch
  set shiftwidth=4
  set et
  " backspace works in insert mode 
  "set bs=start,eol,indent
  " word completion 
  set dictionary=/usr/share/dict/words
  set cin
  set tabstop=4			
  set vb 
  set wim=full
  set wmnu
  set fp=fmt
  set nocp

  " search for the visually selected text
  "vmap X y/<C-R>"<CR>

  " Folding setup 
   set foldmethod=syntax
   set foldlevelstart=20
  " setting my status line so that I do not have to press Ctrl+G to get
  " the damn name of the file 
  set statusline =%<%f%h%m%r%=%l,%c%V\ %P
  " needed by latex suite
  set grepprg=grep\ -nH\ $*
  " keyword seach for labels in tex
  set iskeyword+=:
  let g:Tex_CompileRule_dvi='latex -interaction=nonstopmode --src-specials $*'
  let g:tex_flavor='latex'

  " Optional. This enables automatic indentation as you
  " type.
  filetype indent on
  au BufNewFile,BufRead *.tex  set  spell lbr wm=2 formatoptions=croqn
  au BufNewFile,BufRead *.wiki  set  spell lbr wm=2 formatoptions=croqn
  au BufNewFile,BufRead *.sty  set  lbr wm=2 formatoptions=croqn
  au BufNewFile,BufRead *.txt  set  lbr wm=2 
  au BufNewFile,BufRead *.html  set  lbr wm=2 
  au BufNewFile,BufRead *mutt*  set spell wrap tw=60 lbr formatoptions=aw2tq comments=nb:> nocin
  au BufNewFile,BufRead *.scm,*.ss  set syntax=scheme
  au BufNewFile,BufRead *.beh  set syntax=java
  au BufNewFile,BufRead *.m,*.mi  set tw=120 lbr wm=2 formatoptions=tcrqm syntax=mason
  au BufNewFile,BufRead *.adoc  set syntax=asciidoc
  au BufNewFile,BufRead *.js  set autoindent formatoptions=tcq2l expandtab foldmethod=syntax shiftwidth=2

  augroup pandoc_syntax
      au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
  augroup END


  " This is for the spell checker
  " highlight SpellErrors ctermfg=Red guifg=Red
  "       \ cterm=underline gui=underline term=reverse


  " XML 
  let xml_use_xhtml = 1

  "For the plugin staff
  filetype plugin on 
  "AJC highlighting of packages 
  let ajc_highlight_ajc_lang_ids = 1
  "Java extra highlighting 
  let java_highlight_all = 1  
  let java_highlight_functions = 1  
  
    " Pathogen 
  "execute pathogen#infect()
  " Trying out Plug 
  call plug#begin('~/.vim/bundle')
  " add Plugs here 

"  Plug 'JamshedVesuna/vim-markdown-preview'
  Plug 'scrooloose/nerdtree'
  Plug 'scrooloose/nerdcommenter'
  Plug 'tpope/vim-repeat'
  Plug 'tpope/vim-surround'
  "Plug 'ssh://git.amazon.com:2222/pkg/VimFusion'
"  Plug 'pangloss/vim-javascript'
  Plug 'elzr/vim-json'
  
  "Plug 'ssh://git.amazon.com:2222/pkg/VimIon.git'

  " Snipmate
"  Plug 'SirVer/ultisnips'
"  Plug 'honza/vim-snippets'


  "Pandoc
  " Plug 'vim-pandoc/vim-pandoc'
  " Plug 'vim-pandoc/vim-pandoc-syntax'

  " Tabular 
"  Plug 'dhruvasagar/vim-table-mode'
    Plug 'junegunn/vim-easy-align'
  call plug#end()

" config markdown preview
  "let vim_markdown_preview_browser='Google Chrome' "firefox opens new tab
 " grip for markdown 
  " let vim_markdown_preview_github=1
  let vim_markdown_preview_hotkey='<C-1>'
  let vim_markdown_preview_toggle=1
  let vim_markdown_preview_temp_file=1

" JSON plugin customizations 
  let g:vim_json_syntax_conceal = 0


  "Ultisnips config 
  let g:UltiSnipsExpandTrigger="<tab>"
  let g:UltiSnipsJumpForwardTrigger="<c-b>"
  let g:UltiSnipsJumpBackwardTrigger="<c-z>"
  
  " abbreviations 
  ab my_email   skotthe@ccs.neu.edu
  ab my_name   Therapon Skotiniotis
  ab my_web   http://www.ccs.neu.edu/home.skotthe
  ab ts             --Theo
  ab TS             --Therapon
  " with new lines 
  ab nts             --Theo
  ab NTS             --Therapon 
  ab gc ;;> 
  "ab gc //>

  " Ocaml Merlin 
  set rtp+=/Users/therapon/.opam/4.01.0/share/ocamlmerlin/vim
  set rtp+=/Users/therapon/.opam/4.01.0/share/ocamlmerlin/vimbufsync

  " ocp-indent for ocaml 
  autocmd FileType ocaml source /Users/therapon/.opam/4.01.0/share/vim/syntax/ocp-indent.vim

endif

