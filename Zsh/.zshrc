
export GPG_TTY=$(tty)

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000000
SAVEHIST=10000000
HIST_STAMPS="yyyy-mm-dd"
HISTOR_IGNORE="(ls|cd|pwd|exit|cd)*"
setopt appendhistory autocd extendedglob nomatch notify

setopt EXTENDED_HISTORY      # Write the history file in the ':start:elapsed;command' format.
setopt INC_APPEND_HISTORY    # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY         # Share history between all sessions.
setopt HIST_IGNORE_DUPS      # Do not record an event that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS  # Delete an old recorded event if a new event is a duplicate.
setopt HIST_IGNORE_SPACE     # Do not record an event starting with a space.
setopt HIST_SAVE_NO_DUPS     # Do not write a duplicate event to the history file.
setopt HIST_VERIFY           # Do not execute immediately upon history expansion.
setopt APPEND_HISTORY        # append to history file (Default)
setopt HIST_NO_STORE         # Don't store history commands
setopt HIST_REDUCE_BLANKS    # Remove superfluous blanks from each command line being added to the history.

unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/therapon/.zshrc'


fpath+=~/.zfunc
source ~/Software/Antidote/antidote/antidote.zsh
antidote load 

autoload -Uz compinit
compinit
# End of lines added by compinstall

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
# for pattern search (the wildcard `*` will use zsh completion)
bindkey "^R" history-incremental-pattern-search-backward

autoload -Uz run-help

export PATH=$HOME/bin:$PATH:$HOME/.local/bin:/usr/local/go/bin:$HOME/go/bin
# Homebrew 
export PATH=$PATH:/home/linuxbrew/.linuxbrew/bin
# Latex 
export PATH=$PATH:/usr/local/texlive/2024/bin/x86_64-linux
export MANPATH=$MANPATH:/usr/local/texlive/2024/texmf-dist/doc/man
export INFOPATH=$INFOPATH:/usr/local/texlive/2024/texmf-dist/doc/info

ARCH=$(uname -p)
ARM="arm"
DARWIN="Darwin"

if [[ $ARCH == $ARM ]]; then
	#echo "mac"
	OS=$(uname -s)
else 
	#echo "linux"
	OS=$(uname -o)
    /usr/bin/setxkbmap -option "ctrl:nocaps"
fi


if [[ $ARCH == $ARM && $OS == $DARWIN ]]; then
	echo "Set Gnu bin on ARM"
	PATH=/opt/homebrew/opt/grep/libexec/gnubin:/opt/homebrew/opt/coreutils/libexec/gnubin:/Users/therapon/Library/Python/3.9/bin:$PATH
	export PATH
fi

if [ -f ~/.zsh-aliases ]; then 
	source ~/.zsh-aliases
fi 

if [ -f ~/.cargo/env ]; then 
	source ~/.cargo/env
fi

if [ -f ~/.zsh-texlive ]; then 
	source ~/.zsh-texlive
fi

# Nordvpn 
export NVPN="e9f2ab38fcc1dafd1ff9443660cad039132bdfb1bd1680876312df6f6b86241f"

# openai 
export OPENAI_API_KEY="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"
# Pyenv initialization
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# must be at the end
# eval "$(starship init zsh)"


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if command -v rbenv 1>/dev/null 2>&1; then
  eval "$(rbenv init -)"
fi
#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"


