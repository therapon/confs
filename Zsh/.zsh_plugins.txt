sindresorhus/pure

## Deferred 
zdharma-continuum/fast-syntax-highlighting kind:defer


## Completions 

# zsh-users/zsh-completions is a popular plugin for adding supplemental completions.
# We combine the `path:` and `kind:fpath` annotations here:
zsh-users/zsh-completions path:src kind:fpath

# Compinit plugins should be near the end of .zsh_plugins.txt so that $fpath has been
# fully populated. Use zsh-utils for its completion plugin.
belak/zsh-utils path:completion

# fuzzy search
unixorn/fzf-zsh-plugin

##
## Final Plugins 
zsh-users/zsh-autosuggestions kind:defer
zsh-users/zsh-history-substring-search
